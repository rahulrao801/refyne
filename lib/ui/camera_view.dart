import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:refyne_app/blocs/camera/camera_bloc.dart';
import 'package:refyne_app/my_keys.dart';
import 'package:refyne_app/widgets/error_view.dart';

class CameraView extends StatefulWidget {
  @override
  _CameraViewState createState() => _CameraViewState();
}

class _CameraViewState extends State<CameraView> with WidgetsBindingObserver {
  final globalKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // ignore: close_sinks
    final bloc = BlocProvider.of<CameraBloc>(context);

    //change app state
    if (!bloc.isInitialized()) return;

    if (state == AppLifecycleState.inactive)
      bloc.add(CameraStopped());
    else if (state == AppLifecycleState.resumed) bloc.add(CameraInitialized());
  }

  @override
  Widget build(BuildContext context) => BlocConsumer<CameraBloc, CameraState>(
        builder: (_, state) => Scaffold(
          key: globalKey,
          backgroundColor: Colors.black,
          body: state is CameraReady
              ? SafeArea(
                  key: MyKeys.cameraPreviewScreen,
                  child: Container(
                    child: CameraPreview(
                        BlocProvider.of<CameraBloc>(context).getController()),
                  ),
                )
              : state is CameraFailure
                  ? ErrorView(
                      key: MyKeys.errorScreen,
                      message: state.error,
                    )
                  : Container(),
          floatingActionButton: state is CameraReady
          ? FloatingActionButton(
            onPressed: () => BlocProvider.of<CameraBloc>(context).add(CameraCaptured()),
            child: Icon(
              Icons.camera,
            ),
          )
              : Container(),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        ),
        listener: (_, state) {
          if (state is CameraCaptureSuccess)
            Navigator.of(context).pop(state.path);
          else if (state is CameraCaptureFailure)
            globalKey.currentState.showSnackBar(SnackBar(
              key: MyKeys.errorSnackBar,
              content: Text(state.error),
            ));
        },
      );
}
