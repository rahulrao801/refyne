import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:refyne_app/blocs/camera/camera_bloc.dart';
import 'package:refyne_app/blocs/photos/photos_bloc.dart';
import 'package:refyne_app/ui/camera_view.dart';
import 'package:refyne_app/utils/camera_utils.dart';

class AddView extends StatefulWidget {
  static String route = "/addView";

  @override
  _AddViewState createState() => _AddViewState();
}

class _AddViewState extends State<AddView> {
  String path;

  //upload/ post camera photo taken to api
  void uploadPhoto() {
    BlocProvider.of<PhotosBloc>(context).add(PhotosAdded(file: File(path)));
  }

  //initiate camera
  void openCamera() {
    FocusScope.of(context).requestFocus(FocusNode()); //remove focus
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (_) => CameraBloc(cameraUtils: CameraUtils())
              ..add(CameraInitialized()),
            child: CameraView(),
          ),
        )).then((value) => setState(() => path = value));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Refyne",
          style: TextStyle(fontSize: 20),
        ),
        actions: [
          path != null ? FlatButton.icon(
            textColor: Colors.white,
            icon: Icon(Icons.cloud_upload),
              label: Text(
                'Upload',
              ),
              onPressed: () => uploadPhoto(),
          ): Container(),
        ],
      ),
      body: SafeArea(
        child: Container(
              child: path != null ? Container(
                width: double.infinity,
                child: Image.file(
                  File(path), fit: BoxFit.cover,
                ),
              ) : Container(
                alignment: Alignment.center,
                child: Image.asset(
                  'assets/images/camera_img.png'
                ),
              ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () => openCamera(),
          label: Text(
              'Open Camera',
          ),
        icon: Icon(
          Icons.camera_alt_rounded,
        ),
      ),
    );
  }
}
