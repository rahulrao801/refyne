class PhotoModel {
  List<int> base64;

  PhotoModel({this.base64});

  factory PhotoModel.fromJson(Map<String, dynamic> json) {
    return PhotoModel(
      base64: json['base64'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['base64'] = this.base64;
    return data;
  }
}