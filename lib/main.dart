import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:refyne_app/blocs/photos/photos_bloc.dart';
import 'package:refyne_app/blocs/simple_bloc_observer.dart';
import 'package:refyne_app/locator/locator.dart';
import 'package:refyne_app/ui/add_view.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  //init app service instance for injectable
  setupLocator();
  runApp(BlocProvider(
      create: (context) => PhotosBloc(PhotosInitial()),
      child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: AddView.route,
      routes: {
        AddView.route: (_) => AddView(),
      },
    );
  }
}
