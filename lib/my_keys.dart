import 'package:flutter/material.dart';

class MyKeys {
  static const errorSnackBar = const Key('__errorSnackBar__');
  static const cameraPreviewScreen = const Key('__cameraPreviewScreen__');
  static const errorScreen = const Key('__errorScreen__');
}