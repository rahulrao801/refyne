import 'package:injectable/injectable.dart';
import 'package:refyne_app/repository/api_provider.dart';
import 'package:refyne_app/repository/repository.dart';

//register or inject dependency and its singleton class
@module
abstract class ThirdPartyServicesModule {
  @lazySingleton
  ApiProvider get apiProvider;
  @lazySingleton
  Repository get repository;
}