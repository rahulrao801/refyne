import 'package:flutter/material.dart';
import 'package:refyne_app/locator/locator.dart';
import 'package:refyne_app/model/photo_model.dart';
import 'package:refyne_app/repository/api_provider.dart';

class Repository {
  //instance of api provider
  final ApiProvider _apiProvider = locator<ApiProvider>();

  //method to post base64 image using provider
  Future postImage({@required List<int> base64Photo}) {
    return _apiProvider.postImage(photoModel: PhotoModel(
      base64: base64Photo
    ));
  }
}