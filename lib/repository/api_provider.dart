import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:refyne_app/model/photo_model.dart';

class ApiProvider {
  final Dio _dio;

  //init dio instance with base options
  ApiProvider({Dio dio})
      : _dio = (dio ?? Dio())
    ..options.baseUrl = "https://jsonplaceholder.typicode.com/"
    ..options.contentType = "application/json"
    //access token (currently token hard coded to random string)
    ..options.headers["authorization"] = "Bearer asdkj7862387asad7nbjb}";

  //post request to send image over the network
  Future postImage({@required PhotoModel photoModel}) async {
    try {
      var response = await _dio.post(
        '/posts',
        data: photoModel.toJson()
      );
      return response;
    } on DioError catch (error) {
      return error.response;
    } catch (error) {
      return error;
    }
  }
}