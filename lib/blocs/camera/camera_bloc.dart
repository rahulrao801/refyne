import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:camera/camera.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:refyne_app/utils/camera_utils.dart';

part 'camera_event.dart';
part 'camera_state.dart';

class CameraBloc extends Bloc<CameraEvent, CameraState> {
  final CameraUtils cameraUtils;
  final ResolutionPreset resolutionPreset;
  final CameraLensDirection cameraLensDirection;

  CameraController _cameraController;

  CameraBloc({
    @required this.cameraUtils,
    this.resolutionPreset = ResolutionPreset.high,
    this.cameraLensDirection = CameraLensDirection.back,
  }) : super(CameraInitial());

  CameraController getController() => _cameraController;

  bool isInitialized() => _cameraController?.value?.isInitialized ?? false;

  //camera states
  @override
  Stream<CameraState> mapEventToState(CameraEvent event) async* {
    if (event is CameraInitialized)
      yield* _mapCameraInitializedToState(event);
    else if(event is CameraCaptured)
      yield* _mapCameraCapturedToState(event);
    else if (event is CameraStopped)
      yield* _mapCameraStoppedToState(event);
  }

  //camera initial state setup
  Stream<CameraState> _mapCameraInitializedToState(CameraInitialized event) async* {
    try {
      _cameraController = await cameraUtils.getCameraController(resolutionPreset, cameraLensDirection);
      await _cameraController.initialize();
      yield CameraReady();
    } on CameraException catch (error) {
      _cameraController?.dispose();
      yield CameraFailure(error: error.description);
    } catch (error) {
      yield CameraFailure(error: error.toString());
    }
  }

  //camera captured state
  Stream<CameraState> _mapCameraCapturedToState(CameraCaptured event) async* {
    if (state is CameraReady) {
      yield CameraCaptureInProgress();
      try {
        //final path = await cameraUtils.getPath();
        XFile file = await _cameraController.takePicture();

        yield CameraCaptureSuccess(File(file.path).path);

      } on CameraException catch (error) {
        yield CameraCaptureFailure(error: error.description);
      }
    }
  }

  //on camera stop state
  Stream<CameraState> _mapCameraStoppedToState(CameraStopped event) async* {
    _cameraController?.dispose();
    yield CameraInitial();
  }

  //dispose camera when not in use
  @override
  Future<void> close() {
    _cameraController?.dispose();
    return super.close();
  }

  /*//convert image to Base64
  convertImageToBase64(image) {
    List<int> imageBytes = image.readAsBytesSync();
    String imageB64 = base64Encode(imageBytes);
    Uint8List decoded = base64Decode(imageB64);
    return decoded;
  }*/
}
