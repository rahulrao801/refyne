part of 'camera_bloc.dart';

//events of the camera that is managed

abstract class CameraEvent extends Equatable{
  const CameraEvent();

  @override
  List<Object> get props => [];
}
//on camera initialized event
class CameraInitialized extends CameraEvent{}
//on camera stopped event
class CameraStopped extends CameraEvent{}
//on camera captured event
class CameraCaptured extends CameraEvent{}