part of 'camera_bloc.dart';

//abstract camera state class
abstract class CameraState extends Equatable{
  const CameraState();

  @override
  List<Object> get props => [];
}

//camera initial state
class CameraInitial extends CameraState{}

//on camera ready
class CameraReady extends CameraState{}

//on camera failure
class CameraFailure extends CameraState{
  final String error;

  CameraFailure({this.error = "Camera Failure"});

  @override
  List<Object> get props => [error];
}

//handle in progress state of camera
class CameraCaptureInProgress extends CameraState{}

class CameraCaptureSuccess extends CameraState {
  final String path;

  CameraCaptureSuccess(this.path);
}

//handle state on failure
class CameraCaptureFailure extends CameraReady {
  final String error;

  CameraCaptureFailure({this.error = "Camera Failure"});

  @override
  List<Object> get props => [error];
}