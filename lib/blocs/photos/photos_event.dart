part of 'photos_bloc.dart';

abstract class PhotosEvent extends Equatable {
  const PhotosEvent();


  @override
  List<Object> get props => [];
}

class PhotosAdded extends PhotosEvent {
  final File file;
  PhotosAdded({@required this.file});
}