
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:refyne_app/locator/locator.dart';
import 'package:refyne_app/repository/repository.dart';
import 'package:image/image.dart' as Im;

part 'photos_event.dart';

part 'photos_state.dart';

class PhotosBloc extends Bloc<PhotosEvent, PhotosState> {
  final Repository _repository = locator<Repository>();

  PhotosBloc(PhotosState initialState) : super(initialState);

  @override
  Stream<PhotosState> mapEventToState(PhotosEvent event) async*{
    if(event is PhotosAdded)
      yield* _mapPhotosAddedToState(event);
  }

  Stream<PhotosState> _mapPhotosAddedToState(PhotosAdded event) async* {
      yield PhotosLoadInProgress();
      try {
        //get total file size
        var fileLength = await event.file.length();

        //compress image if its more then 4mb
        if((fileLength/1048576) > 4.0){
          Im.Image image = Im.decodeImage(event.file.readAsBytesSync());
          var compressedImage = File(event.file.path)..writeAsBytesSync(Im.encodeJpg(image, quality: 85));
          var base64Photo = convertImageToBase64(compressedImage);
          //Api call to post the base64 image
          await _repository.postImage(base64Photo: base64Photo);
          yield PhotosLoadSuccess(success: 'Photos Success');
        } else {
          var base64Photo = convertImageToBase64(File(event.file.path));
          //Api call to post the base64 image
          await _repository.postImage(base64Photo: base64Photo);
          yield PhotosLoadSuccess(success: 'Photos Success');
        }
        /*await ;*/
        yield PhotosLoadSuccess(success: 'Photos Success');
      } on Exception catch (error) {
        yield PhotosLoadFailure(error: error.toString());
      }
  }

  //convert image to Base64
  convertImageToBase64(image) {
    List<int> imageBytes = image.readAsBytesSync();
    String imageB64 = base64Encode(imageBytes);
    Uint8List decoded = base64Decode(imageB64);
    return decoded;
  }
}