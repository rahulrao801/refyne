part of 'photos_bloc.dart';

abstract class PhotosState extends Equatable {
  const PhotosState();

  @override
  List<Object> get props => [];
}

class PhotosInitial extends PhotosState {}

class PhotosLoadInProgress extends PhotosState {}

class PhotosLoadSuccess extends PhotosState {
  final String success;
  PhotosLoadSuccess({this.success = "Photos Success"});
}

class PhotosLoadFailure extends PhotosState {
  final String error;

  PhotosLoadFailure({this.error = "Photos Failure"});

  @override
  List<Object> get props => [error];
}