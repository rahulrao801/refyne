import 'package:flutter/material.dart';

class ErrorView extends StatelessWidget {
  final String message;

  ErrorView({Key key, this.message = "Camera Error"}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.error,
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
    );
  }
}
