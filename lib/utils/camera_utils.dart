import 'package:camera/camera.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class CameraUtils {
  //get access to available cameras in the device
  Future<CameraController> getCameraController(
      ResolutionPreset resolutionPreset,
      CameraLensDirection cameraLensDirection,
      ) async {
    //provides the list of available camera option based on the phone
    final availableCamera = await availableCameras();

    //selecting first instance of the direction the camera is facing
    final camera = availableCamera.firstWhere((camera) => camera.lensDirection == cameraLensDirection);

    //initialising the camera and its resolution to camera controller
    return CameraController(camera, resolutionPreset, enableAudio:  false);
  }

  //get path of the image stored
  Future<String> getPath() async => join(
    (await getTemporaryDirectory()).path,
    '${DateTime.now()}.png'
  );
}